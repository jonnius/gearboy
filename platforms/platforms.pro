TEMPLATE = subdirs

android {
    message("Building android version")
    SUBDIRS += android
}

ubuntu_touch {
    message("Building ubuntu phone version")
    SUBDIRS += ubuntu_touch
}

desktop {
    message("Building generic version")
    SUBDIRS += desktop
}

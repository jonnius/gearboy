Gearboy-Qt
==========
This is a fork of Ignacio Sanchez's Gearboy, focusing on Qt/QML and OpenGL ES for mobile phones.

<img src="https://github.com/RyanPattison/Gearboy/blob/master/platforms/ubuntu_touch/gearboy.png" width="64"> 
<img src="https://github.com/RyanPattison/Gearboy/blob/master/platforms/ubuntu_touch/screenshots/landscape.png" width="256">
<img src="https://github.com/RyanPattison/Gearboy/blob/master/platforms/ubuntu_touch/screenshots/portrait.png" height="256">

Build Instructions
===================

### Ubuntu Touch

  1. Install [Clickable](http://clickable.bhdouglass.com/en/latest/)
  2. Run `clickable -c platforms/ubuntu_touch/clickable.json`

### Android & Desktop

Build these with Qt Creator and the project files under platforms. These versions exist to test on a variety of hardware and are not (yet) supported.
